import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';

import VacationPlanner from './app/VacationPlanner'

export default class App extends Component {
    render() {
        return(
            <VacationPlanner></VacationPlanner>
        );
    }
}