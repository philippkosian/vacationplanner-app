import { StyleSheet } from 'react-native';

export default mainstyle = StyleSheet.create({
    wrap: {
        marginTop: 0, 
        flex: 1, 
        flexDirection: 'column',
        alignItems: 'stretch'
    },

    spaceTop: {
        marginTop: 120
    }
});