import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { AppRegistry } from 'react-native';

import Login from './components/Login/Login';
import Register from './components/Register/Register';
import Calendar from './components/Calendar/Calendar';

import mainstyle from './mainstyle';


export default class VacationPlannerApp extends Component {

    render() {
        return (
            <Router>
                <Scene key="root">
                    <Scene  key="login"
                            component={Login}
                            title="Login"
                            initial/>
                    <Scene  key="register"
                            component={Register}
                            title="Register"/>
                    <Scene  key="calendar"
                            component={Calendar}
                            title="Calendar"/>
                </Scene>
            </Router>
        );
    }

}

AppRegistry.registerComponent('VacationPlannerApp', () => VacationPlannerApp);