import React from 'react';
import { Alert } from 'react-native';

export default class BackendHelper {

    constructor() {
        this.url = "https://services.publicis.de/vacation-plan/app/appbackend.php";
    }

    dbFetch(that, request, callback) {

        let header = {  method: 'POST', 
                        headers: {  'Accept': 'application/json', 
                                    'Content-Type': 'application/json' },
                        body: JSON.stringify( request )
        };

        fetch( this.url, header )
            .then((response) => response.json())
            .then((responseJson) => {
                callback(that, responseJson, request);
            })
            .catch((error) => {
                console.log(error);
                Alert.alert("Network Error", "Error log: " + error);
            });
    }
}