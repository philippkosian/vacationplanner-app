import { StyleSheet } from 'react-native';

export default style = StyleSheet.create({

    wrap: {
    },

    topWrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    userName: {
        flex: 0.3,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 10,
        textAlign: "center",
        fontSize: 16,
        fontWeight: "bold"
    },

    userGroupLabel: {
        flex: 0.2,
        marginLeft: 10,
        marginRight: 5,
        textAlign: "right",
        fontSize: 16
    },

    pickerWrap: {
        flex: 0.5,
        alignSelf: "flex-end",
        overflow: "hidden",
    },

    picker: {
        justifyContent: "center",
        height: 60
    },

    pickerItem: {
    },

    mainNav: {
        flexDirection: "row",
        justifyContent: "space-between", 
        marginBottom: 10,
        marginRight: 15,
        marginLeft: 15
    },

    mainDate: {
        alignSelf: "center"
    },

    tableWrap: {
        flex: 1
    },

    loading: {
        alignSelf: "center",
        marginTop: 30
    }
});