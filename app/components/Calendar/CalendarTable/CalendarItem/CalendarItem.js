import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Text, View, TouchableHighlight, Alert } from 'react-native';

import style from './style' ;


export default class CalendarItem extends Component {


    constructor(props) {
        super(props);

        this.state = {
            day:        this.props.day, 
            row:        this.props.row,
            user:       this.props.user,
            value:      this.props.value,
            editable:   this.props.editable,
            toggled:    false
        }
    }

    componentWillReceiveProps(nextProps) {
    }

    render() {

        let valueStyle = {};
        if(this.state.value == 'unavailable') {
            valueStyle = style.valueUnavailable;
        } else {
            if(!this.state.toggled) {
                valueStyle = style.valueAvailable;
                if(this.state.editable) {
                    valueStyle = style.valueEditable;
                }
                if(this.state.value == 'planned') {
                    valueStyle = style.valuePlanned;
                }
            } else {
                valueStyle = style.valueAvailableHighlight;
                if(this.state.value == 'planned') {
                    valueStyle = style.valuePlannedHighlight;
                }
            }
        }


        if(!this.state.editable || this.state.value == 'unavailable') {
            return(
                <TouchableHighlight style={ [valueStyle, style.calendarItem, this.props.dimensions] }
                                    underlayColor={null}>
                    <View></View>
                </TouchableHighlight>
            );
        } else {
            return(
                <TouchableHighlight style={ [valueStyle, style.calendarItem, this.props.dimensions] }
                                    onPress={ this._onPress.bind(this) }
                                    underlayColor={null}>
                   <View></View>
                </TouchableHighlight>
            );
        }

        
    }

    _onPress() {
        if(this.state.value != 'unavailable' && this.state.editable) {
            this.props.onPress(this.state.row, this.state.day, this.state.value, this.state.user);
            this.setState({
                toggled: true
            });
        } else {
            Alert.alert("Huh?!");
        }
    }

/*
    changeState(vals) {

        let possibleKeys = ['user', 'value', 'toggled'];

        //check if key in "vals" is in possible keys
        for(let i=0; i < Object.keys(vals).length; i++) {

            let key = Object.keys(vals)[i];
            let found = false;

            for(let j=0; j < possibleKeys.length; j++) {
                let stateKey = possibleKeys[j];
                if(!found && key == stateKey) {
                    found = true;
                }
            }

            if(!found) {
                console.log('"'+key+'" is not a valid key for changing item state.');
            }
        }

        // PROBLEM: when changing toggled to "false", no value is set, because check (vals['toggled']) isn't met
        this.setState({
            user: vals['user'] ? vals['user'] : this.state.user,
            value: vals['value'] ? vals['value'] : this.state.value,
            toggled: vals['toggled'] ? vals['toggled'] : this.state.toggled
        });
    }
*/


}