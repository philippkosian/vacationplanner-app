import { StyleSheet } from 'react-native';

var hc1 = '#337ab7';

export default style = StyleSheet.create({

    calendarItem: {
        alignItems: "center", 
        justifyContent: "center",
        borderRightWidth: 1,
        borderBottomWidth: 1
    },

    highlight: {
        backgroundColor: '#aaa'
    },

    itemText: {
        color: '#000'
    }, 

    valueAvailable: {
        backgroundColor: '#eaeaea'
    }, 

    valueAvailableHighlight: {
        backgroundColor: '#aaa'
    }, 

    valuePlanned: {
        backgroundColor: '#0e0'
    }, 

    valuePlannedHighlight: {
        backgroundColor: '#090'
    }, 

    valueUnavailable: {
        backgroundColor: hc1
    },

    valueEditable: {
        backgroundColor: '#fdfdfd'
    }

});