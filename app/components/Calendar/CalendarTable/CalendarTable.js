import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, Text, ScrollView, Alert, ListView, PanResponder, Animated, TouchableHighlight, Easing } from 'react-native';

import CalendarItem from './CalendarItem/CalendarItem';

import style from './style' ;




export default class CalendarTable extends Component {


    constructor(props) {

        super(props);

        //Dimensions/Values
        this.boxWidth = 70;
        this.boxHeight = 45;
        this.dateRowHeight = 25;
        this.nameColumnMinWidth = 40;

        this.usertype_admin = 1;
        this.usertype_standard = 0;

        this.tableInnerWidthPanMax = 0;
        this.tableInnerHeightPanMax = 0;
        this.retractionSpeed = 0.15;

        this.weekdays = {
            0: "Su",
            1: "Mo",
            2: "Tu",
            3: "We",
            4: "Th", 
            5: "Fr",
            6: "Sa"
        };


        this._calculateDynamicStyles(this.props.users, this.props.data, this.props.daysInMonth);


        this.state = {
            selectedMonth:      this.props.selectedMonth,
            selectedYear:       this.props.selectedYear,
            daysInMonth:        this.props.daysInMonth,
            unavailableDays:    this.props.unavailableDays,
            users:              this.props.users,
            data:               this.props.data,
            key:                Math.random(),
            panValues:          new Animated.ValueXY(),
            nameColumnWidth:    new Animated.Value(this.boxWidth),
            nameColumnOpacity:  new Animated.Value(1),
            nameColumnOpacity2: new Animated.Value(0),
            menuHeight:         new Animated.Value(0),
            menuWrapOpacity:    new Animated.Value(0),
            selectedItem:       false,
            timespanStart:      false
        };

    }


    // Called when CalendarTable is rerendered, receives and sets new props
    componentWillReceiveProps(nextProps) {
        this._calculateDynamicStyles(nextProps.users, nextProps.data, nextProps.daysInMonth);
        
        this.setState({
            selectedMonth:      nextProps.selectedMonth,
            selectedYear:       nextProps.selectedYear,
            daysInMonth:        nextProps.daysInMonth,
            unavailableDays:    nextProps.unavailableDays,
            users:              nextProps.users,
            data:               nextProps.data,
            key:                Math.random(),
            panValues:          new Animated.ValueXY(),
            nameColumnWidth:    new Animated.Value(this.boxWidth),
            nameColumnOpacity:  new Animated.Value(1),
            nameColumnOpacity2: new Animated.Value(0),
            selectedItem:       false,
            timespanStart:      false
        });
    }


    _calculateDynamicStyles(users, data, daysInMonth) {
        this.tableInnerWidth = daysInMonth * this.boxWidth;
        this.tableInnerHeight = users.length * this.boxHeight;

        //Dynamic Styles
        this.boxWidthStyle = {
            width:  this.boxWidth
        };
        this.boxHeightStyle = {
            height: this.boxHeight
        };

        this.dateRowHeightStyle = {
            height: this.dateRowHeight
        };
        this.dateRowDimensionsStyle = {
            width:  this.tableInnerWidth + this.boxWidth,
            height: this.dateRowHeight
        };

        this.nameColumnHeightStyle = {
            height: this.tableInnerHeight
        };

        this.tableInnerDimensionsStyle = {
            width:  this.tableInnerWidth,
            height: this.tableInnerHeight
        };
    }


// PanResponder Initialization

    componentWillMount() {

        this._panResponder = PanResponder.create({

            onStartShouldSetPanResponder: () => true,

            onMoveShouldSetPanResponderCapture: () => true,

            onPanResponderGrant: (e, gestureState) => {
                this.state.panValues.flattenOffset();
                this.state.panValues.setOffset({
                    x: this.state.panValues.x._value, 
                    y: this.state.panValues.y._value
                });
                this.state.panValues.setValue({x: 0, y: 0});
            },

            onPanResponderMove: (e, gestureState) => {

                // 0,0 is upper left corner

                // Delta of gesture
                let gesdx = gestureState.dx,
                    gesdy = gestureState.dy;
                
                // Potential offset if gesture were applied as is
                let potOffx = this.state.panValues.x._offset + gesdx,
                    potOffy = this.state.panValues.y._offset + gesdy;


                // Potential Offset too far left (too low)
                if(potOffx > 0) {
                    // Change gesture to stay still when reaching the edge
                    // -> workaround so next gesture always amounts to offset: 0
                    gesdx = -(this.state.panValues.x._offset);
                } 
                // Potential offset too far right (too high)
                else if(potOffx < -(this.tableInnerWidthPanMax)) {
                    gesdx = -(this.tableInnerWidthPanMax) - this.state.panValues.x._offset;
                }
                // Ceiling (y too low)
                if(potOffy > 0) {
                    gesdy = -(this.state.panValues.y._offset);
                }
                // Bottom (y too high)
                else if(potOffy < -(this.tableInnerHeightPanMax)) {
                    // if small height (tableInnerHeightPanMax is below zero if table doesn't fill out the wrap)
                    if(this.tableInnerHeightPanMax < 0) {
                        // -> workaround so next gesture always amounts to offset: 0
                        gesdy = -(this.state.panValues.y._offset);
                    } else {
                        // -> workaround so next gesture always amounts to max possible offset
                        gesdy = -(this.tableInnerHeightPanMax) - this.state.panValues.y._offset;
                    }
                }


                // Animates by the gesture
                Animated.event(
                    [null, {
                        dx: this.state.panValues.x,
                        dy: this.state.panValues.y,
                    }]
                )(e, {dx: gesdx, dy: gesdy});



                // Animate NameColumnWidth
                // TODO

                // this.retractionSpeed <---> gestureState.vx


                // Extract:
                if( potOffx >= 0 ) {
                    this._extract();
                }
                // Contract:
                if( potOffx < 0 ) {  
                    this._contract();
                }

                
            }, 

            onPanResponderRelease: (e, {vx, vy}) => {
                // console.log(vxNew, vyNew);
                
                // Animate coasting by vx, vy (decay?)
                    // avoid coasting over border

                this.state.panValues.flattenOffset(); // maybe not necessary
            },
        });
    }


    // Username menu contraction mechanism

    _extract() {
        Animated.parallel([
            Animated.timing( this.state.nameColumnWidth, {
                toValue: this.boxWidth,
                easing: Easing.linear
            }),
            Animated.timing(
                this.state.nameColumnOpacity, {
                    toValue: 1
            })
        ]).start();
    }

    _contract() {
        Animated.parallel([
            Animated.timing( this.state.nameColumnWidth, { 
                toValue: this.nameColumnMinWidth,
                easing: Easing.linear
            }),
            Animated.timing( this.state.nameColumnOpacity, { 
                toValue: 0
            })
        ]).start();
    }



    render() {

        /*if(this.state.daysInMonth.length != this.state.data[Object.keys(this.state.data)[0]].length) {
            Alert.alert("Data and month do not match.");
        }*/

        let panXY = this.state.panValues.getLayout(),
            panX = {left: this.state.panValues.x},
            panY = {top: this.state.panValues.y};
        let nameColumnWidthStyle = {width: this.state.nameColumnWidth};

        //console.log(this._createTable());

        // If children get pushed from a function, 
        //   always assign the parent view a property from state,
        //   Otherwise the children won't be rerendered

        return (
            <Animated.View  style={ style.wrap } { ...this._panResponder.panHandlers }>
                <View style={ style.topPart }>
                    <Animated.View style={ [nameColumnWidthStyle, this.dateRowHeightStyle, style.dateBox, style.dateBoxStatic] }>
                        <Text>{this.state.users.length}\{this.state.daysInMonth}</Text>
                    </Animated.View>
                    <View style={ style.dateBoxWrap }>
                        <Animated.View  style={ [panX, this.dateRowDimensionsStyle, style.dateRow] }>
                            { this._createDateRow() }
                        </Animated.View>
                    </View>
                </View>

                <View style={ style.bottomPart }>
                    <View style={ style.nameColumn }>
                        <Animated.View  style={ [panY, nameColumnWidthStyle, this.nameColumnHeightStyle] }>
                            { this._createNameColumn() }
                        </Animated.View>
                    </View>
                    <View style={ style.table }
                          onLayout={(event) => this._measureInnerWrap(event)}>
                        <Animated.View  style={ [panXY, this.tableInnerDimensionsStyle, style.tableInner] }>
                            { this._createTable() }
                        </Animated.View>
                    </View>
                </View>
                { this._createItemMenu() }
            </Animated.View>
        );
    }


    _measureInnerWrap(event) {
        this.tableInnerWidthPanMax = this.tableInnerWidth - event.nativeEvent.layout.width;
        this.tableInnerHeightPanMax = this.tableInnerHeight - event.nativeEvent.layout.height;
    }


    _createDateRow() {
        var view = [];

        for(var i=1; i <= this.state.daysInMonth; i++) {
            let id = new Date(this.state.selectedYear, this.state.selectedMonth-1, i).getDay();
            let weekday = this.weekdays[id];
            view.push(
                <View style={ [this.boxWidthStyle, this.dateRowHeightStyle, style.dateBox] }
                        key={ "datebox-"+i }>
                    <Text>{weekday} {i}.</Text>
                </View>
            );
        }
        return view;
    }


    _createNameColumn() {
        var view = [];
        let opacity = {
            opacity: (this.state.nameColumnOpacity)
        }
        let opacityFlipped = {
            opacity: (this.state.nameColumnOpacity.interpolate({inputRange: [0, 1], outputRange: [1, 0], extrapolate: 'clamp'}))
        }

        for(var i=0; i < this.state.users.length; i++) {
            view.push(
                <View style={ [this.boxHeightStyle, style.nameBox] }
                      key={ [this.state.nameColumnIsSmall, i] }>
                    <Animated.View style={ [opacity, style.nameColumnTextWrap] }>
                        <Text style={ style.nameColumnText }>{ this.state.users[i].name }</Text>
                    </Animated.View>
                    <Animated.View style={ [opacityFlipped, style.nameColumnTextWrap] }>
                        <Text style={ style.nameColumnText }>{ this.state.users[i].initial }</Text>
                    </Animated.View>
                </View>
            );
        }
        return view;
    }


    _createTable() {
        let view = [];
        for(let r=0; r < this.state.users.length; r++) {

            let editable = false;
            // if logged in user is admin
            if(this.props.loginResult.type == this.usertype_admin) {
                editable = true;
            // if logged in user is user of current row and standard user (allowed to edit himself)
            } else if(this.state.users[r].id == this.props.loginResult.id && this.props.loginResult.type == this.usertype_standard) {
                editable = true;
            }

            view.push(
                <View style={ [style.tableRow, this.boxHeightStyle] }
                        key={ ["row-"+r , this.state.key] }>
                    { this._createTableRow(r, editable) }
                </View>
            );
        }
        return view;
    }


    _createTableRow(r, editable) {
        let userID = this.state.users[r].id;
        let user = this.state.users[r];

        let view = [];
        // create calendarItem for every day in month
        for(let i=0; i < this.state.daysInMonth; i++) {

            let day = i+1;

            let value = 'available';
            // day is weekend or feiertag
            if(this._inArray(day, this.state.unavailableDays)) {
                value = 'unavailable';
            // day is already planned for user
            } else if(this._dayIsPlannedForUser(userID, day)) {
                value = 'planned';
            }

            view.push(
                <CalendarItem   day={ day }
                                row={ r+1 }
                                user={ user }
                                value={ value }
                                dimensions={ {width: this.boxWidth, height: this.boxHeight} }
                                ref={ "item-"+r+"-"+i }
                                onPress={ this._onItemPress.bind(this) }
                                editable={ editable }
                                key={ "item-"+r+"-"+i }
                />
            );

        }
        return view;
    }


    _createItemMenu() {

        let heightStyle = {
            height: this.state.menuHeight
        }
        let backgroundOpacity = {
            backgroundColor: 'rgba(0,0,0,0.3)',
            opacity: this.state.menuWrapOpacity
        }

        if(this.state.selectedItem) {

            let selectedDateText = this.state.selectedYear + "/" + this._toTwoCharNum(this.state.selectedMonth) + "/" + this._toTwoCharNum(this.state.selectedItem.day);

            if(this.state.timespanStart && this.state.timespanStart.user.id == this.state.selectedItem.user.id) {
                // Menu for Timespan (User of box is user of start of timespan, no user-overlapping timespan allowed!)

                let timespanStartText = this.state.selectedYear + "/" + this._toTwoCharNum(this.state.selectedMonth) + "/" + this._toTwoCharNum(this.state.timespanStart.day);

                // check if timespan will add or remove vacation
                let timespanActionText = "End timespan and add/remove vacation";
                if(this._checkTimespanAllVacation()) {
                    timespanActionText = "Remove vacation in timespan";
                } else {
                    timespanActionText = "Add vacation for timespan";
                }

                return(
                    <Animated.View style={ [backgroundOpacity, style.itemMenuWrap] }>
                        <TouchableHighlight onPress={ this._closeItemMenu.bind(this) }
                                            style={ style.itemMenuOverlay }
                                            underlayColor='rgba(0,0,0,0.2)'>
                            <View></View>
                        </TouchableHighlight>
                        <Animated.View style={ [style.itemMenu, heightStyle] }>
                            <Text style={ style.itemMenuHeadline }>
                                { this.state.selectedItem.user.name + ": " + timespanStartText + "  -  " + selectedDateText }
                            </Text>
                            <TouchableHighlight style={ style.itemMenuButton }
                                                onPress={ this._setVacationEnd.bind(this) }
                                                underlayColor={ '#888' }
                                                key={'timespan-end-set-btn'}>
                                <Text style={ style.itemMenuButtonText }>{ timespanActionText }</Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={ style.itemMenuButton }
                                                onPress={ this._setVacationStart.bind(this) }
                                                underlayColor={ '#888' }>
                                <Text style={ style.itemMenuButtonText }>Start timespan here</Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={ style.itemMenuButton }
                                                onPress={ this._resetVacationStart.bind(this) }
                                                underlayColor={ '#888' }>
                                <Text style={ style.itemMenuButtonText }>Reset timespan</Text>
                            </TouchableHighlight>
                        </Animated.View>
                    </Animated.View>
                );

            } else {
                // Menu without Timespan

                return(
                    <Animated.View style={ [backgroundOpacity, style.itemMenuWrap] }>
                        <TouchableHighlight onPress={ this._closeItemMenu.bind(this) }
                                            style={ style.itemMenuOverlay }
                                            underlayColor='rgba(0,0,0,0.2)'>
                            <View></View>
                        </TouchableHighlight>
                        <Animated.View style={ [style.itemMenu, heightStyle] }>
                            <Text style={ style.itemMenuHeadline }>
                                { this.state.selectedItem.user.name + ": " + selectedDateText }
                            </Text>
                            <TouchableHighlight style={ style.itemMenuButton }
                                                onPress={ this._setVacationDay.bind(this) }
                                                underlayColor={ '#888' }>
                                <Text style={ style.itemMenuButtonText }>
                                    {   (this._dayIsPlannedForUser(this.state.selectedItem.user.id, this.state.selectedItem.day)) ? "Remove vacation" : "Set vacation" }
                                </Text>
                            </TouchableHighlight>
                            <TouchableHighlight style={ style.itemMenuButton }
                                                onPress={ this._setVacationStart.bind(this) }
                                                underlayColor={ '#888' }>
                                <Text style={ style.itemMenuButtonText }>Start timespan here</Text>
                            </TouchableHighlight>
                        </Animated.View>
                    </Animated.View>
                );
            }
        }
    }

    componentDidUpdate() {
        this._animateMenu();
    }


    _animateMenu() {
        if(this.state.selectedItem) {
            if(this.state.timespanStart && this.state.timespanStart.user.id == this.state.selectedItem.user.id) { // Timespan Menu
                Animated.parallel([
                    Animated.timing( this.state.menuWrapOpacity, { 
                        toValue: 1,
                        duration: 200
                    }),
                    Animated.timing( this.state.menuHeight, { 
                        toValue: 185,
                        duration: 300
                    })
                ]).start();
            } else {
                Animated.parallel([
                    Animated.timing( this.state.menuWrapOpacity, { 
                        toValue: 1,
                        duration: 200
                    }),
                    Animated.timing( this.state.menuHeight, { 
                        toValue: 135,
                        duration: 300
                    })
                ]).start();
            }
        }
    }


    _onItemPress(row, day, value, user){
        if(day == this.state.selectedItem.day && row == this.state.selectedItem.row) {
            this._closeItemMenu();
        } else {
            this._selectItem(row, day, user);
        }
    }

    _selectItem(row, day, user) {
        this.setState({
            selectedItem: {row: row, day: day, user: user}
        });
    }

    _closeItemMenu() {
        let r = this.state.selectedItem.row - 1;
        let d = this.state.selectedItem.day - 1;
        let itemRef = "item-"+r+"-"+d;

        if(this.refs[itemRef]) {
            this.refs[itemRef].setState({toggled: false});
        }

        Animated.parallel([
            Animated.timing( this.state.menuWrapOpacity, { 
                toValue: 0,
                duration: 200
            }),
            Animated.timing( this.state.menuHeight, { 
                toValue: 0,
                duration: 300
            })
        ]).start( () => this.setState({ selectedItem: false }) );
        
    }

    _inArray(target, array) {
        for(var i = 0; i < array.length; i++) {
            if(array[i] === target) {
                return true;
            }
        }
        return false; 
    }

    _dayIsPlannedForUser(userID, day) {
        if(this.state.data) {
            for(let i=0; i < this.state.data.length; i++) {
                if(     userID == this.state.data[i].user_id
                        && this.state.data[i].day == day) {
                    return true;
                }
            }
        }
        return false;
    }



    _setVacationDay() {
        // if day is not unavailable
        if(!this._inArray(this.state.selectedItem.day, this.state.unavailableDays)) {
            // remove from database if already planned
            if(this._dayIsPlannedForUser(this.state.selectedItem.user.id, this.state.selectedItem.day)) {
                this.props.writeVacationDay(this.state.selectedItem.day, this.state.selectedItem.user, "delete-date", 1);
            // otherwise add day
            } else {
                this.props.writeVacationDay(this.state.selectedItem.day, this.state.selectedItem.user, "write-date", 1);
            }
        }
        
        this._closeItemMenu();
    }


    _setVacationStart() {
        this.setState({
            timespanStart: {day: this.state.selectedItem.day, user: this.state.selectedItem.user}
        });
        this._closeItemMenu();
    }

    _resetVacationStart() {
        this.setState({
            timespanStart: false
        });
        this._closeItemMenu();
    }

    _checkTimespanAllVacation() {

        let dayStart = this.state.timespanStart.day;
        let dayEnd = this.state.selectedItem.day;
        let user = this.state.selectedItem.user;

        // Switch dayStart and End if end is smaller than start
        if(dayStart > dayEnd) {
            let dayStartNew = dayEnd;
            dayEnd = dayStart;
            dayStart = dayStartNew;
        }

        // check if all selected days are vacation
        let allVacation = true;
        for(let i=dayEnd; i >= dayStart; i--) {
            // uanvailable days don't count
            if(!this._inArray(i, this.state.unavailableDays)) {
                // set flag to false if an available day is found
                if(!this._dayIsPlannedForUser(user.id, i)) {
                    allVacation = false;
                }
            }
        }

        return allVacation;
    }

    _setVacationEnd() {

        let dayStart = this.state.timespanStart.day;
        let dayEnd = this.state.selectedItem.day;
        let user = this.state.selectedItem.user;

        // Switch dayStart and End if end is smaller than start
        if(dayStart > dayEnd) {
            let dayStartNew = dayEnd;
            dayEnd = dayStart;
            dayStart = dayStartNew;
        }

        // check if all selected days are vacation
        let allVacation = true;
        for(let i=dayEnd; i >= dayStart; i--) {
            // uanvailable days don't count
            if(!this._inArray(i, this.state.unavailableDays)) {
                // set flag to false if an available day is found
                if(!this._dayIsPlannedForUser(user.id, i)) {
                    allVacation = false;
                }
            }
        }



        // if all selected days (except for unavailable days) are already vacation, remove vacation on every one of them
            // will also remove falsey vacation days on weekends
        if(allVacation) {
            // collect days that will be sent
            days = [];
            for(let i=dayEnd; i >= dayStart; i--) {
                days.push(i);
            }
            // send commands to Calendar
            for(let i=0; i < days.length; i++) {
                if(i == 0) {
                    this.props.writeVacationDay(days[i], this.state.selectedItem.user, "delete-date", days.length);
                } else {
                    this.props.writeVacationDay(days[i], this.state.selectedItem.user, "delete-date");
                }
            }
        // otherwise fill in every day as vacation that's not already set
        } else {
            // collect days that will be sent
            days = [];
            for(let i=dayEnd; i >= dayStart; i--) {
                if(!this._dayIsPlannedForUser(user.id, i) && !this._inArray(i, this.state.unavailableDays)) {
                    days.push(i);
                }
            }

            // send commands to Calendar
            for(let i=0; i < days.length; i++) {
                if(i == 0) {
                    this.props.writeVacationDay(days[i], this.state.selectedItem.user, "write-date", days.length);
                } else {
                    this.props.writeVacationDay(days[i], this.state.selectedItem.user, "write-date");
                }
            }
        }
            

        this.setState({
            timespanStart: false
        });
        this._closeItemMenu();
    }

    _toTwoCharNum(num) {
        let newNum = '';
        if(num < 10) {
            newNum = "0"+num.toString();
        } else {
            newNum = num.toString();
        }
        return newNum;
    }

}