import { StyleSheet } from 'react-native';

var bc1 = '#e8e8e8';
var bc2 = '#c8c8d0';
var hc1 = '#337ab7';

export default style = StyleSheet.create({

    wrap: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-start", 
        borderWidth: 1,
        overflow: "hidden"
    },

    topPart: {
        borderBottomWidth: 1,
        flexDirection: "row", 
        paddingLeft: 1,
        backgroundColor: bc2,
        zIndex: 3,
    },

    dateBoxStatic: {
        backgroundColor: bc2,
        zIndex: 4,
    },

    dateRow: {
        flexDirection: "row",
        zIndex: 3,
    },

    dateBoxWrap: {
        backgroundColor: bc2,
        zIndex: 3,
    },

    dateBox: {
        justifyContent: "center", 
        alignItems: "center", 
        borderRightWidth: 1,
        zIndex: 3
    },

    bottomPart: {
        flex: 1, 
        flexDirection: "row", 
        justifyContent: "flex-start", 
        alignItems: "stretch",
        zIndex: 2,
    },

    nameColumn: {
        backgroundColor: bc2, 
        borderRightWidth: 1,
        zIndex: 2,
    },

    nameBox: {
        flexDirection: "column",
        justifyContent: "center",
        borderBottomWidth: 1,
        zIndex: 2,
    },

    nameColumnTextWrap: {
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 2,
    },

    nameColumnText: {
        textAlign: 'center',
        textAlignVertical: 'center',
        margin: 5,
        fontSize: 12,
        zIndex: 2,
    },

    table: {
        flex: 1,
    }, 

    tableInner: {
        zIndex: 1,
    },

    tableRow: {
        flexDirection: "row",
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },

    itemMenuWrap: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 99
    }, 

    itemMenuOverlay: {
        flex: 1
    },

    itemMenu: {
        bottom: 0,
        borderTopWidth: 1,
        borderColor: '#bbb'
    }, 

    itemMenuHeadline: {
        backgroundColor: '#ddd',
        height: 35,
        fontWeight: 'bold',
        fontSize: 14,
        paddingTop: 10,
        textAlign: 'center'
    },

    itemMenuButton: {
        backgroundColor: '#fff',
        height: 50, 
        borderTopWidth: 1,
        borderColor: '#bbb'
    }, 

    itemMenuButtonText: {
        height: 50,
        fontWeight: 'bold',
        fontSize: 18,
        textAlign: 'center',
        paddingTop: 12,
    }

});