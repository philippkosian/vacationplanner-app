import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, Text, Button, Alert, Picker } from 'react-native';

import BackendHelper from './../BackendHelper/BackendHelper';
import CalendarTable from './CalendarTable/CalendarTable';

import mainstyle from './../../mainstyle';
import style from './style' ;



export default class Calendar extends Component {

    constructor(props) {
        super(props);

        this.dbHelper = new BackendHelper();

        // Constants
        this.standardUsergroup = 0;
        


        // Logged in user: this.props.loginResult

        // get date informations
        let date = new Date();
        let startYear = date.getFullYear();
        let startMonth = date.getMonth()+1;
        let daysInMonth = this._calcDaysInMonth(startYear, startMonth);
        let unavailableDays = this._calcUnavailableDays(startYear, startMonth, daysInMonth);


        // Query from database
        this._getUsers();
        this._getDataForMonth(startYear, startMonth);
        this._getUsergroups();

        this.state = {
            selectedMonth:      startMonth, 
            selectedYear:       startYear, 
            daysInMonth:        daysInMonth,
            unavailableDays:    unavailableDays,
            data:               null, 
            users:              null, 
            usergroups:         null,
            dbstack:            0,
            activeUsergroup:    this.standardUsergroup
        }
    }


    render() {

        return (
            <View   style={ [mainstyle.wrap, style.wrap] }>

                <View   style={ style.topWrap }>
                    <Text   style={ style.userName }>{ this.props.loginResult.name }</Text>
                    <Text   style={ style.userGroupLabel }>Group: </Text>
                    <View style={ style.pickerWrap }>
                        <Picker selectedValue={ this.state.activeUsergroup }
                                onValueChange={ (group) => this._changeUsergroup(group) }
                                style={ style.picker }
                                backgroundColor="#66A">
                            { this._renderUsergroupItems() }
                        </Picker>
                    </View>
                </View>

                <View   style={ style.mainNav }>
                    <Button title="    <<    "
                            onPress={ this._prevMonth.bind(this) }
                            color="#337ab7"/>
                    <Text   style={ style.mainDate }>{ this.state.selectedMonth + " / " + this.state.selectedYear }</Text>
                    <Button title="    >>    "
                            onPress={ this._nextMonth.bind(this) }
                            color="#337ab7"/>
                </View>

                <View   style={ style.tableWrap }>
                    { this._renderTable() }
                </View>

            </View>
        )
    }


    _renderUsergroupItems() {
        if(this.state.usergroups) {
            var output = [];
            for(let usergroup of this.state.usergroups) {
                output.push(
                    <Picker.Item    label={usergroup[1]} 
                                    value={usergroup[0]} 
                                    style={style.pickerItem} 
                                    key={"usergroup-label-" + usergroup[0]}
                    />
                );
            }
            return output;
        } else {
            return;
        }
    }


    // Update functions
    // =================


    _renderTable() {
        if(this.state.data && this.state.users && this.state.dbstack == 0 && this.state.usergroups) {
            return(
                <CalendarTable  loginResult={ this.props.loginResult }
                                selectedMonth={ this.state.selectedMonth }
                                selectedYear={ this.state.selectedYear }
                                daysInMonth={ this.state.daysInMonth }
                                unavailableDays={ this.state.unavailableDays }
                                users={ this.state.users }
                                data={ this.state.data }
                                writeVacationDay={ this._writeVacationDay.bind(this) }
                                updateData={ this._updateData.bind(this) }/>
            );
        } else {
            return(
                <Text style={ style.loading }>Loading...</Text>
            );
        }
    }

    _updateData(cmdDone) {
        
        // if this was the last command in dbstack, update data
        if(this.state.dbstack == 1) {
            this._getUsers();
            this._getDataForMonth(this.state.selectedYear, this.state.selectedMonth);
        }

        // count dbstack down by one if cmdDone was set
        if(cmdDone) {
            this._countCmdStackDown();
        }

    }

    _prevMonth() {
        let newSelectedMonth = this.state.selectedMonth - 1;
        let newSelectedYear = this.state.selectedYear;

        if(newSelectedMonth < 1) {
            newSelectedMonth = 12;
            newSelectedYear = newSelectedYear - 1;
        }

        this._updateStateForMonth(newSelectedYear, newSelectedMonth);
    }

    _nextMonth() {
        let newSelectedMonth = this.state.selectedMonth + 1;
        let newSelectedYear = this.state.selectedYear;

        if(newSelectedMonth > 12) {
            newSelectedMonth = 1;
            newSelectedYear = newSelectedYear + 1;
        }

        this._updateStateForMonth(newSelectedYear, newSelectedMonth);
    }

    _updateStateForMonth(newSelectedYear, newSelectedMonth) {

        // get days in next month
        let daysInMonth = this._calcDaysInMonth(newSelectedYear, newSelectedMonth);
        let unavailableDays = this._calcUnavailableDays(newSelectedYear, newSelectedMonth, daysInMonth);

        this.setState({
            selectedMonth:      newSelectedMonth,
            selectedYear:       newSelectedYear, 
            daysInMonth:        daysInMonth,
            unavailableDays:    unavailableDays, 
            data:               null                // set to null so loading screen appears
        });

        // get new data
        this._getDataForMonth(newSelectedYear, newSelectedMonth);
    }

    _countCmdStackDown() {
        let dbstack = this.state.dbstack-1;
        this.setState({
            dbstack: dbstack
        });
    }

    _changeUsergroup(group) {
        this._getUsers(group);

        this.setState({
            users: null,
            activeUsergroup: group
        });
    }

    _calcUnavailableDays(year, month, days) {
        let unavail = [];

        // Iteration through days of selected month
        for(let i=1; i <= days; i++) {
            // Calculate weekends
            let day = new Date(year, month-1, i);
            if(day.getDay() == 0 || day.getDay() == 6) {
                unavail.push(i);
            }
        }


        // Static public holidays
        let staticPublicHolidays = [
            [1, 1],     // Neujahrstag
            [1, 6],     // Heilige Drei Könige
            [5, 1],     // Tag der Arbeit
            [8, 15],    // Mariä Himmelfahrt
            [10, 3],    // Tag der Deutschen Einheit
            [11, 1],    // Allerheiligen
            [12, 25],   // Erster Weihnachtstag
            [12, 26],   // Zweiter Weihnachtstag)
        ];
        for(let i=0; i < staticPublicHolidays.length; i++) {
            if(staticPublicHolidays[i][0] == month) {
                unavail.push(staticPublicHolidays[i][1]);
            }
        }

        // Variable public holidays
        let easter = this._easter(year);

        variablePublicHolidays = [
            easter,                                     // Ostern
            this._createDateAfterEaster(easter, -2),    // Karfreitag
            this._createDateAfterEaster(easter, 1),     // Ostermontag
            this._createDateAfterEaster(easter, 39),    // Christi Himmelfahrt
            this._createDateAfterEaster(easter, 50),    // Pfingstmontag
            this._createDateAfterEaster(easter, 60)     // Fronleichnam
        ];
        for(let i=0; i < variablePublicHolidays.length; i++) {
            if(variablePublicHolidays[i].getMonth()+1 == month) {
                unavail.push(variablePublicHolidays[i].getDate());
            }
        }


        return unavail;
    }


    // API Queries
    // ===================

    _getDataForMonth(year, month) {

        dataRequest = {
            "login": this.props.loginResult.loginRequest.login,
            "action": "get-dates",
            "yearmonth": year+"-"+this._toTwoCharNum(month)
        }

        this.dbHelper.dbFetch(this, dataRequest, function(that, data, dataRequest) {            
            for(let i in data){
                data[i].day = that._dayOfDateString(data[i].date);
            }
            that.setState({
                data: data
            });
        });
    }

    _getUsers(usergroup=this.standardUsergroup) {

        userRequest = {
            "login": this.props.loginResult.loginRequest.login,
            "action": "get-users",
            "usergroup": usergroup
        }

        this.dbHelper.dbFetch(this, userRequest, function(that, users, userRequest){
            if(users) {
                // add initials
                for(let i in users) {
                    if(users.hasOwnProperty(i)) {
                        users[i].initial = that._createUserInitial(users[i].name) + ".";
                    }
                }

                // set state
                that.setState({
                    users: users
                });
            } else {
                Alert.alert("Backend Error", "There might be a problem with the server or database.")
                // console.log(users);
            }
            
        });
    }

    _getUsergroups() {
        userRequest = {
            "login": this.props.loginResult.loginRequest.login,
            "action": "get-usergroups"
        }

        this.dbHelper.dbFetch(this, userRequest, function(that, groups, userRequest){
            if(groups) {
                // set state
                that.setState({
                    usergroups: groups
                });
            } else {
                Alert.alert("Backend Error", "There might be a problem with the server or database.")
                // console.log(groups);
            }
            
        });
    }

    _writeVacationDay(dayOfMonth, user, action, stack=false) {

        stateUpdate = {
            data: null,
            users: null
        }

        if(stack) {
            stateUpdate.dbstack = stack;
        }

        this.setState(stateUpdate);
        

        let date = this._dateStringOfDay(dayOfMonth);

        writeRequest = {
            "login": this.props.loginResult.loginRequest.login,
            "action": action,
            "user_id": user.id,
            "date": date
        }

        this.dbHelper.dbFetch(this, writeRequest, function(that, res, writeRequest){
            if(res[0]) {
                that._updateData(true);
            } else {
                Alert.alert("Backend Error.", "Backend encountered an error while processing the command.");
                that._updateData(true);
            }
        });
        
    }



    // Helper functions
    // ==================

    _calcDaysInMonth(year, month) {
        return new Date(year, month, 0).getDate();
    }

    _dayOfDateString(datestr) {
        let daystr = datestr.substr(8,10);
        return parseInt(daystr);
    }

    _dateStringOfDay(dayOfMonth) {
        if(dayOfMonth) {
            return this.state.selectedYear + "-" + this._toTwoCharNum(this.state.selectedMonth) + "-" + this._toTwoCharNum(dayOfMonth);
        }
    }

    _toTwoCharNum(num) {
        let newNum = '';
        if(num < 10) {
            newNum = "0"+num.toString();
        } else {
            newNum = num.toString();
        }
        return newNum;
    }

    _copy2dArray(array) {
        let newArray = [];
        for(let i=0; i < array.length; i++) {
            newArray[i] = array[i].slice();
        }
        return newArray;
    }

    _createUserInitial(name) {
        return name.substring(0,2)
    }

    _easter(Y) {
        var C = Math.floor(Y/100);
        var N = Y - 19*Math.floor(Y/19);
        var K = Math.floor((C - 17)/25);
        var I = C - Math.floor(C/4) - Math.floor((C - K)/3) + 19*N + 15;
        I = I - 30*Math.floor((I/30));
        I = I - Math.floor(I/28)*(1 - Math.floor(I/28)*Math.floor(29/(I + 1))*Math.floor((21 - N)/11));
        var J = Y + Math.floor(Y/4) + I + 2 - C + Math.floor(C/4);
        J = J - 7*Math.floor(J/7);
        var L = I - J;
        var M = 3 + Math.floor((L + 40)/44);
        var D = L + 28 - 31*Math.floor(M/4);

        return new Date(Y, M-1, D);
    }

    _createDateAfterEaster(easter, days) {
        let date = new Date(easter.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }

}