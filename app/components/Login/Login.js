import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, Text, TextInput, Button, TouchableHighlight, Alert, Keyboard } from 'react-native';

import BackendHelper from './../BackendHelper/BackendHelper';

import mainstyle from './../../mainstyle';
import style from './style' ;



export default class Login extends Component {

    constructor(props) {
        super(props);

        this.emailInput = "";
        this.passwordInput = "";

        this.dbHelper = new BackendHelper();

        this.state = {
            loading: false
        };
    }

    

    render() {
        return (
            <View style={ [mainstyle.wrap, mainstyle.spaceTop, style.wrap] }>
                <TextInput  placeholder="E-Mail"
                            defaultValue={this.emailInput}
                            // autoFocus = { true }
                            returnKeyType = { "next" }
                            onChangeText = {
                                (text) => this.emailInput = text
                            }
                            onSubmitEditing = {
                                (event) => this.refs.passwordInput.focus()
                            }
                />
                <TextInput  ref="passwordInput"
                            defaultValue={this.passwordInput}
                            placeholder="Password"
                            onChangeText = {
                                (text) => this.passwordInput = text
                            }
                            secureTextEntry={true}
                            onSubmitEditing = {
                                this._onLoginButtonPress.bind(this)
                            }
                />

                { this._loadingHint() }

                <View   style={ style.loginButtonWrap }>
                    <Button onPress={
                                this._onLoginButtonPress.bind(this)
                            }
                            title="Login"
                    />
                </View>

            </View>
        )
    }


    _loadingHint() {
        if(this.state.loading) {
            return(<Text>Logging in, please wait...</Text>);
        } else {
            return(<Text> </Text>);
        }
    }

    _onLoginButtonPress() {

        Keyboard.dismiss()
        
        if(this.emailInput) {
            if(this.passwordInput) {

                let loginRequest = {
                    "login": {
                        "email": this.emailInput,
                        "password": this.passwordInput
                    },
                    "action": "login"
                };

                this.dbHelper.dbFetch(this, loginRequest, function(that, res, loginRequest) {

                    if(res[0] != false) {
                        res.loginRequest = loginRequest;
                        Actions.calendar({loginResult: res});   // Open calendar and pass result
                    } else {
                        Alert.alert("Login Error", "Please check your credentials.");
                    }

                    that.setState({             // Loading finishes in callback function
                        loading: false
                    })
                    
                })

                this.setState({             // Loading starts after query was sent
                    loading: true
                });

            } else {
                Alert.alert("Password not set");
            }
        } else {
            Alert.alert("E-Mail not set");
        }

    }

}