import { StyleSheet } from 'react-native';

export default style = StyleSheet.create({

    wrap: {
        marginRight: 25,
        marginLeft: 25
    },

    loginButtonWrap: {
        marginTop: 30,
        marginBottom: 0
    },

    registerButtonWrap: {
        marginTop: 30
    }
});