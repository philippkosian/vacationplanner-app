import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { View, Button, TextInput, Text, Alert } from 'react-native';

import mainstyle from './../../mainstyle';
import style from './style' ;



export default class Register extends Component {

    constructor(probs) {
        super(probs);

        var emailInput = null;
        var nameInput = null;
        var passwordInput = null;
        var passwordInputRepeat = null;
    }

    _onRegisterButtonPress() {

        // TODO: Check if pw and pwrepeat are the same

        Alert.alert("E-Mail: "+this.emailInput+", Name: "+this.nameInput);
        if(this.passwordInput) {
            Alert.alert("Password was set.");
        } else {
            Alert.alert("Password not set.");
        }
    }

    render() {
        return (
            <View style={ [mainstyle.wrap, mainstyle.spaceTop, style.wrap] }>
                <Text>Coming soon. Please use the web platform to sign up in the mean time.</Text>
                <TextInput  placeholder="E-Mail"
                            onChangeText={
                                (text) => this.emailInput = text
                            }
                            onSubmitEditing = {
                                (event) => this.refs.nameInput.focus()
                            }
                />
                <TextInput  ref="nameInput"
                            placeholder="Name"
                            onChangeText={
                                (text) => this.nameInput = text
                            }
                            onSubmitEditing = {
                                (event) => this.refs.passwordInput.focus()
                            }
                />
                <TextInput  ref="passwordInput"
                            placeholder="Password"
                            onChangeText={
                                (text) => this.passwordInput = text
                            }
                            secureTextEntry={true}
                />
                <TextInput  ref="passwordInputRepeat"
                            placeholder="Repeat Password"
                            onChangeText={
                                (text) => this.passwordInputRepeat = text
                            }
                            secureTextEntry={true}
                />
                <View       style={ style.registerButtonWrap }>
                    <Button     onPress={ 
                                    this._onRegisterButtonPress.bind(this)
                                } 
                                title="Register"
                                disabled={true}
                    />
                </View>
            </View>
        )
    }

}