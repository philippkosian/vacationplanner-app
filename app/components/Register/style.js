
import { StyleSheet } from 'react-native';

export default style = StyleSheet.create({
    wrap: {
        marginRight: 25,
        marginLeft: 25
    },
    
    registerButtonWrap: {
        marginTop: 30
    }
});